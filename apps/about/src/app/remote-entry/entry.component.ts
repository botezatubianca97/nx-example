import { Component } from '@angular/core';

@Component({
  selector: 'micro-frontends-about-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss'],
})
export class RemoteEntryComponent {}
