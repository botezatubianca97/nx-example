import { Component } from '@angular/core';

@Component({
  selector: 'micro-frontends-shop-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss'],
})
export class RemoteEntryComponent {}
